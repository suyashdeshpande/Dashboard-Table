import {Component, OnInit, ViewChild} from '@angular/core';
import {GenericTableComponent, GtConfig} from '@angular-generic-table/core';
import {DashboardService} from '../dashboard.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  configObject: GtConfig<any> = {
    settings: [],
    fields: [],
    data: []
  };
  paymentObject: GtConfig<any> = {
    settings: [],
    fields: [],
    data: []
  };

constructor(public _service: DashboardService) {
  let temp = this._service.getRegistered();

}
  @ViewChild('registeredTable') registeredTable: GenericTableComponent<any, any>;
  @ViewChild('paymentTable') paymentTable: GenericTableComponent<any, any>;

  data = {
    'header': [{
      'title': 'Id',
      'key': 'id'
    },
      {
        'title': 'Registered',
        'key': 'registered'
      },
      {
        'title': 'Username',
        'key': 'username'
      },
      {
        'title': 'Type',
        'key': 'type'
      },
      {
        'title': 'Email',
        'key': 'email'
      },
      {
        'title': 'Profession',
        'key': 'profession'
      }, {
        'title': 'Plan Date',
        'key': 'plan.date'
      },
      {
        'title': 'Plan Type',
        'key': 'plan.type'
      },
      {
        'title': 'Plan Amount',
        'key': 'plan.amount'
      }, {
        'title': 'Plan Cancellation Date',
        'key': 'plan.cancellation.date'
      },
      {
        'title': 'Plan Activity Upgrade',
        'key': 'plan.activity'
      },
      {
        'title': 'Plan Activity Downgrade',
        'key': 'plan.activity.da'
      },
    ],
    'rows': [
      {
        'id': '5b35f5e6000e3cf0ca6f1ec5',
        'registered': 'Thu Apr 27 2017 08:09:13 GMT+0000',
        'username': 'Maura',
        'type': 'Individual',
        'email': 'maura@overfork.biz',
        'profession': 'GYM',
        'plan': {
          'date': 'Fri Jun 29 2018 07:06:24 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,906.41',
          'cancellation': {
            'date': 'Tue Dec 30 2014 05:16:11 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed Dec 31 2014 18:42:31 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Dec 21 2016 21:42:14 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6531ad6b7cdfc311f',
        'registered': 'Sun Aug 06 2017 23:14:11 GMT+0000',
        'username': 'Betty',
        'type': 'Individual',
        'email': 'betty@centuria.io',
        'profession': 'Salon',
        'plan': {
          'date': 'Mon Feb 22 2016 15:49:11 GMT+0000',
          'type': 'Growth',
          'amount': '$3,677.13',
          'cancellation': {
            'date': 'Sat Apr 28 2018 18:50:41 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Tue Sep 09 2014 01:27:47 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Mon Feb 22 2016 07:33:18 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e60101ef8274c2cb07',
        'registered': 'Thu Mar 10 2016 11:06:05 GMT+0000',
        'username': 'Kelley',
        'type': 'Individual',
        'email': 'kelley@centrexin.ca',
        'profession': 'GYM',
        'plan': {
          'date': 'Wed Feb 03 2016 18:30:38 GMT+0000',
          'type': 'Growth',
          'amount': '$3,173.11',
          'cancellation': {
            'date': 'Wed Sep 21 2016 06:24:10 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sun Mar 22 2015 09:42:40 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Mar 11 2015 15:07:36 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6a3f4b44b97afd773',
        'registered': 'Tue Jan 02 2018 04:58:28 GMT+0000',
        'username': 'Ana',
        'type': 'Enterprise',
        'email': 'ana@fortean.com',
        'profession': 'GYM',
        'plan': {
          'date': 'Wed May 06 2015 05:27:45 GMT+0000',
          'type': 'Enterprise',
          'amount': '$3,284.02',
          'cancellation': {
            'date': 'Fri Jul 24 2015 05:35:33 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Fri Feb 20 2015 00:59:09 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Jul 08 2015 14:36:49 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e60a12f0e57d233256',
        'registered': 'Thu Jul 28 2016 09:12:01 GMT+0000',
        'username': 'Moon',
        'type': 'Individual',
        'email': 'moon@snips.info',
        'profession': 'GYM',
        'plan': {
          'date': 'Fri Jun 12 2015 14:40:58 GMT+0000',
          'type': 'Enterprise',
          'amount': '$3,971.60',
          'cancellation': {
            'date': 'Fri Aug 11 2017 16:14:39 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sun Feb 18 2018 00:36:40 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Mon May 26 2014 00:44:46 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6e74920da327a81a1',
        'registered': 'Thu Mar 16 2017 23:50:30 GMT+0000',
        'username': 'Clayton',
        'type': 'Individual',
        'email': 'clayton@imperium.tv',
        'profession': 'GYM',
        'plan': {
          'date': 'Thu Jun 02 2016 04:33:56 GMT+0000',
          'type': 'Enterprise',
          'amount': '$2,617.38',
          'cancellation': {
            'date': 'Sun Mar 05 2017 08:07:57 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed Jul 12 2017 19:53:12 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Thu Mar 02 2017 02:42:52 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6206affba588a319e',
        'registered': 'Sat Mar 22 2014 22:24:42 GMT+0000',
        'username': 'Oneal',
        'type': 'Individual',
        'email': 'oneal@zepitope.name',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Fri Dec 16 2016 21:29:07 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,999.32',
          'cancellation': {
            'date': 'Fri May 23 2014 15:22:15 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu Apr 13 2017 00:38:04 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Tue Jan 17 2017 12:55:19 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e62158eabeba6a9e0b',
        'registered': 'Mon Oct 26 2015 06:21:32 GMT+0000',
        'username': 'Levine',
        'type': 'Individual',
        'email': 'levine@pyrami.co.uk',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Sun Dec 14 2014 11:11:07 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,948.76',
          'cancellation': {
            'date': 'Mon Oct 03 2016 08:22:12 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Tue Nov 24 2015 14:49:54 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Thu Apr 10 2014 14:59:03 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e611c552cf057067cf',
        'registered': 'Sun Jan 08 2017 22:09:17 GMT+0000',
        'username': 'Kemp',
        'type': 'Enterprise',
        'email': 'kemp@zillidium.org',
        'profession': 'GYM',
        'plan': {
          'date': 'Sat May 12 2018 21:42:18 GMT+0000',
          'type': 'Enterprise',
          'amount': '$2,508.90',
          'cancellation': {
            'date': 'Sun Apr 29 2018 21:35:45 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu Jul 16 2015 15:06:36 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Fri May 19 2017 18:21:15 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e664fd0b7d3c2b8c5d',
        'registered': 'Sat May 30 2015 08:30:02 GMT+0000',
        'username': 'Perkins',
        'type': 'Enterprise',
        'email': 'perkins@quadeebo.us',
        'profession': 'Salon',
        'plan': {
          'date': 'Wed May 03 2017 15:12:41 GMT+0000',
          'type': 'Growth',
          'amount': '$1,731.21',
          'cancellation': {
            'date': 'Sun Nov 19 2017 19:06:46 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sat May 28 2016 20:38:22 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Tue Apr 15 2014 19:18:53 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6383e6de8fdb0ea84',
        'registered': 'Thu Sep 10 2015 14:42:57 GMT+0000',
        'username': 'Alejandra',
        'type': 'Enterprise',
        'email': 'alejandra@helixo.net',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Wed Apr 08 2015 19:59:38 GMT+0000',
          'type': 'Growth',
          'amount': '$3,320.23',
          'cancellation': {
            'date': 'Sat Jul 22 2017 01:55:47 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sun Jul 16 2017 16:12:42 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Jul 16 2014 09:41:18 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6fdf9ead7e37059ef',
        'registered': 'Tue Sep 20 2016 01:06:15 GMT+0000',
        'username': 'Claire',
        'type': 'Enterprise',
        'email': 'claire@syntac.me',
        'profession': 'GYM',
        'plan': {
          'date': 'Sun Aug 23 2015 02:33:33 GMT+0000',
          'type': 'Growth',
          'amount': '$1,070.05',
          'cancellation': {
            'date': 'Sat Apr 19 2014 15:16:56 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu Jun 26 2014 22:17:51 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Mon Mar 27 2017 18:51:24 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e670e088615170fb79',
        'registered': 'Fri Dec 19 2014 17:06:43 GMT+0000',
        'username': 'Molina',
        'type': 'Enterprise',
        'email': 'molina@mondicil.biz',
        'profession': 'GYM',
        'plan': {
          'date': 'Fri Feb 07 2014 17:14:19 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,834.90',
          'cancellation': {
            'date': 'Mon Jun 12 2017 03:40:40 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Mon Jun 08 2015 05:17:35 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Sep 24 2014 16:36:10 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e697408cf14f492343',
        'registered': 'Mon May 26 2014 17:07:37 GMT+0000',
        'username': 'Betsy',
        'type': 'Individual',
        'email': 'betsy@roboid.io',
        'profession': 'GYM',
        'plan': {
          'date': 'Sun Mar 25 2018 04:01:03 GMT+0000',
          'type': 'Growth',
          'amount': '$3,057.46',
          'cancellation': {
            'date': 'Mon Feb 12 2018 01:55:39 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed Jun 13 2018 23:52:24 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Mon May 29 2017 06:55:12 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6cdb5f48eb6b42031',
        'registered': 'Wed Jun 20 2018 23:16:35 GMT+0000',
        'username': 'Tyler',
        'type': 'Individual',
        'email': 'tyler@organica.ca',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Mon Apr 24 2017 08:06:07 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,302.37',
          'cancellation': {
            'date': 'Fri Aug 12 2016 19:20:00 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Fri Dec 22 2017 04:28:08 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Thu Mar 27 2014 20:08:32 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e60c5cc34ab6c096ec',
        'registered': 'Tue Apr 19 2016 14:00:49 GMT+0000',
        'username': 'Fran',
        'type': 'Enterprise',
        'email': 'fran@zedalis.com',
        'profession': 'GYM',
        'plan': {
          'date': 'Sat Nov 07 2015 18:37:29 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,549.78',
          'cancellation': {
            'date': 'Sat Jan 27 2018 12:27:47 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu Jun 28 2018 19:06:54 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Jun 03 2015 19:04:38 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e669ac636b4ace018c',
        'registered': 'Sun Jan 17 2016 09:07:56 GMT+0000',
        'username': 'Dorothea',
        'type': 'Individual',
        'email': 'dorothea@comvoy.info',
        'profession': 'GYM',
        'plan': {
          'date': 'Sun Feb 05 2017 13:01:18 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,536.72',
          'cancellation': {
            'date': 'Mon Dec 22 2014 10:47:36 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sat Jan 21 2017 22:43:57 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Mon Feb 08 2016 16:43:39 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6c45af2a63374c779',
        'registered': 'Wed Sep 14 2016 09:23:28 GMT+0000',
        'username': 'Lela',
        'type': 'Enterprise',
        'email': 'lela@extragen.tv',
        'profession': 'Salon',
        'plan': {
          'date': 'Mon Oct 16 2017 10:51:35 GMT+0000',
          'type': 'Growth',
          'amount': '$3,548.96',
          'cancellation': {
            'date': 'Wed Oct 18 2017 11:01:59 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Fri Aug 26 2016 15:00:02 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Sat Feb 28 2015 13:45:25 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6ec2f74085105a86b',
        'registered': 'Sun Mar 19 2017 21:54:34 GMT+0000',
        'username': 'Mona',
        'type': 'Enterprise',
        'email': 'mona@hivedom.name',
        'profession': 'Salon',
        'plan': {
          'date': 'Wed Mar 15 2017 13:42:09 GMT+0000',
          'type': 'Enterprise',
          'amount': '$2,867.62',
          'cancellation': {
            'date': 'Sun Jun 08 2014 21:22:44 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed Sep 07 2016 14:27:15 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Thu Feb 20 2014 17:29:46 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6711d99c192500e4f',
        'registered': 'Thu Nov 26 2015 22:29:01 GMT+0000',
        'username': 'Dotson',
        'type': 'Individual',
        'email': 'dotson@earthmark.co.uk',
        'profession': 'GYM',
        'plan': {
          'date': 'Tue Mar 22 2016 17:15:23 GMT+0000',
          'type': 'Growth',
          'amount': '$2,408.85',
          'cancellation': {
            'date': 'Thu Apr 23 2015 14:28:12 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed May 23 2018 21:32:15 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Tue Jan 12 2016 18:50:26 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6b13a74b6297ffb3d',
        'registered': 'Thu Sep 10 2015 10:59:25 GMT+0000',
        'username': 'Jodie',
        'type': 'Enterprise',
        'email': 'jodie@tetratrex.org',
        'profession': 'Salon',
        'plan': {
          'date': 'Wed Aug 12 2015 10:33:20 GMT+0000',
          'type': 'Growth',
          'amount': '$1,733.51',
          'cancellation': {
            'date': 'Fri Aug 11 2017 12:16:33 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed Mar 01 2017 08:08:01 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Tue Apr 29 2014 07:28:10 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6e5fa9d255ff9a330',
        'registered': 'Tue Apr 04 2017 15:36:27 GMT+0000',
        'username': 'Terrie',
        'type': 'Individual',
        'email': 'terrie@sulfax.us',
        'profession': 'Salon',
        'plan': {
          'date': 'Tue Oct 17 2017 21:05:04 GMT+0000',
          'type': 'Growth',
          'amount': '$2,058.95',
          'cancellation': {
            'date': 'Tue Jan 30 2018 12:24:24 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Fri Jan 10 2014 06:23:01 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Sat Nov 28 2015 00:29:09 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6d11a05071766c121',
        'registered': 'Sat Aug 23 2014 13:40:41 GMT+0000',
        'username': 'Jordan',
        'type': 'Enterprise',
        'email': 'jordan@krog.net',
        'profession': 'GYM',
        'plan': {
          'date': 'Tue Feb 14 2017 05:38:38 GMT+0000',
          'type': 'Growth',
          'amount': '$3,422.82',
          'cancellation': {
            'date': 'Mon Jun 23 2014 11:53:42 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu Oct 09 2014 17:38:50 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Tue May 29 2018 11:13:16 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e64e16390655ef90f2',
        'registered': 'Thu Aug 28 2014 02:52:05 GMT+0000',
        'username': 'Dina',
        'type': 'Individual',
        'email': 'dina@lunchpod.me',
        'profession': 'Salon',
        'plan': {
          'date': 'Mon Mar 23 2015 08:36:32 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,260.65',
          'cancellation': {
            'date': 'Thu Jan 14 2016 21:23:56 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Tue Apr 24 2018 07:11:30 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Mon Mar 09 2015 05:38:56 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e682c97306c59dc219',
        'registered': 'Thu Oct 05 2017 18:54:10 GMT+0000',
        'username': 'Corrine',
        'type': 'Enterprise',
        'email': 'corrine@obliq.biz',
        'profession': 'GYM',
        'plan': {
          'date': 'Fri Jul 21 2017 13:03:07 GMT+0000',
          'type': 'Enterprise',
          'amount': '$2,962.20',
          'cancellation': {
            'date': 'Thu Oct 12 2017 01:08:46 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Fri Feb 09 2018 03:46:06 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Mon Oct 05 2015 12:42:30 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e69cbf5af4104b8702',
        'registered': 'Tue Mar 07 2017 04:53:16 GMT+0000',
        'username': 'Buchanan',
        'type': 'Individual',
        'email': 'buchanan@viocular.io',
        'profession': 'GYM',
        'plan': {
          'date': 'Sat Jun 03 2017 21:38:17 GMT+0000',
          'type': 'Enterprise',
          'amount': '$2,267.57',
          'cancellation': {
            'date': 'Mon Sep 25 2017 17:43:45 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu May 19 2016 11:16:40 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Thu Nov 30 2017 08:36:08 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e69912a6690b412ccf',
        'registered': 'Mon Feb 06 2017 16:19:28 GMT+0000',
        'username': 'Guthrie',
        'type': 'Enterprise',
        'email': 'guthrie@moltonic.ca',
        'profession': 'Salon',
        'plan': {
          'date': 'Tue Apr 24 2018 11:23:54 GMT+0000',
          'type': 'Growth',
          'amount': '$3,165.23',
          'cancellation': {
            'date': 'Tue Jan 14 2014 14:23:03 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Fri Jun 24 2016 11:35:49 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Nov 30 2016 10:34:30 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6f547c6f5966ea4fc',
        'registered': 'Tue Oct 13 2015 10:04:56 GMT+0000',
        'username': 'Cecelia',
        'type': 'Individual',
        'email': 'cecelia@yurture.com',
        'profession': 'GYM',
        'plan': {
          'date': 'Sat May 12 2018 07:37:30 GMT+0000',
          'type': 'Enterprise',
          'amount': '$3,031.09',
          'cancellation': {
            'date': 'Sun Jan 04 2015 21:24:18 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Mon Jul 31 2017 09:39:52 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Sun Aug 27 2017 08:14:27 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6cc38f55126eb1f60',
        'registered': 'Fri Jul 17 2015 05:21:19 GMT+0000',
        'username': 'Stefanie',
        'type': 'Enterprise',
        'email': 'stefanie@primordia.info',
        'profession': 'Salon',
        'plan': {
          'date': 'Sat May 23 2015 01:30:57 GMT+0000',
          'type': 'Enterprise',
          'amount': '$2,724.13',
          'cancellation': {
            'date': 'Tue Sep 08 2015 20:05:15 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed Jan 03 2018 04:15:16 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Sat Apr 16 2016 23:51:01 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e643f08372f7e5e7ac',
        'registered': 'Wed Jul 26 2017 08:12:27 GMT+0000',
        'username': 'Alexandra',
        'type': 'Enterprise',
        'email': 'alexandra@biflex.tv',
        'profession': 'Salon',
        'plan': {
          'date': 'Wed Apr 25 2018 19:50:44 GMT+0000',
          'type': 'Enterprise',
          'amount': '$2,270.18',
          'cancellation': {
            'date': 'Thu Feb 19 2015 15:01:05 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Tue Jun 16 2015 19:13:05 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Mon Feb 05 2018 07:00:08 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6706f289bf5d3de08',
        'registered': 'Wed Jun 14 2017 08:43:06 GMT+0000',
        'username': 'Janelle',
        'type': 'Enterprise',
        'email': 'janelle@lexicondo.name',
        'profession': 'Salon',
        'plan': {
          'date': 'Sat Aug 05 2017 06:50:22 GMT+0000',
          'type': 'Growth',
          'amount': '$3,597.59',
          'cancellation': {
            'date': 'Sat Apr 21 2018 08:01:01 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu Aug 24 2017 01:36:08 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Thu Aug 04 2016 15:43:45 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6c365c7cac0dc3027',
        'registered': 'Sat Aug 19 2017 04:24:38 GMT+0000',
        'username': 'Burgess',
        'type': 'Individual',
        'email': 'burgess@liquidoc.co.uk',
        'profession': 'Salon',
        'plan': {
          'date': 'Sun Oct 11 2015 13:38:50 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,162.25',
          'cancellation': {
            'date': 'Sat Apr 08 2017 06:40:25 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu Jun 09 2016 22:57:22 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Tue Aug 22 2017 14:23:17 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6febba901cc8b38f1',
        'registered': 'Sun Oct 11 2015 22:02:35 GMT+0000',
        'username': 'Osborn',
        'type': 'Individual',
        'email': 'osborn@kyagoro.org',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Thu Apr 09 2015 15:05:30 GMT+0000',
          'type': 'Growth',
          'amount': '$2,131.84',
          'cancellation': {
            'date': 'Sat Oct 10 2015 08:40:27 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sat May 21 2016 17:21:09 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Sun Mar 13 2016 21:23:23 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e69709e29c82f151ac',
        'registered': 'Tue Jun 26 2018 18:57:46 GMT+0000',
        'username': 'Parsons',
        'type': 'Individual',
        'email': 'parsons@furnitech.us',
        'profession': 'GYM',
        'plan': {
          'date': 'Wed Nov 15 2017 02:52:53 GMT+0000',
          'type': 'Growth',
          'amount': '$3,488.03',
          'cancellation': {
            'date': 'Sun Jan 17 2016 17:58:30 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed Mar 08 2017 20:58:06 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Tue Feb 09 2016 20:56:38 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6e1f4f1f09bf80014',
        'registered': 'Mon Apr 16 2018 13:08:59 GMT+0000',
        'username': 'Nichole',
        'type': 'Enterprise',
        'email': 'nichole@freakin.net',
        'profession': 'GYM',
        'plan': {
          'date': 'Wed Nov 01 2017 02:46:57 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,463.22',
          'cancellation': {
            'date': 'Fri Dec 02 2016 05:40:27 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed Jun 06 2018 12:49:00 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Nov 08 2017 14:51:59 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6e7aedd4f5ff04812',
        'registered': 'Thu Feb 19 2015 06:43:44 GMT+0000',
        'username': 'Odonnell',
        'type': 'Individual',
        'email': 'odonnell@geekfarm.me',
        'profession': 'Salon',
        'plan': {
          'date': 'Mon Feb 06 2017 09:33:07 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,451.04',
          'cancellation': {
            'date': 'Sat Jan 25 2014 15:57:57 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Tue Mar 08 2016 04:48:48 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Thu Jan 30 2014 17:00:20 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e685a1a914a3584e92',
        'registered': 'Fri Feb 26 2016 04:44:46 GMT+0000',
        'username': 'Atkins',
        'type': 'Enterprise',
        'email': 'atkins@biospan.biz',
        'profession': 'GYM',
        'plan': {
          'date': 'Sat Oct 25 2014 18:38:07 GMT+0000',
          'type': 'Growth',
          'amount': '$2,511.56',
          'cancellation': {
            'date': 'Tue Oct 24 2017 11:42:49 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Mon Dec 08 2014 20:49:00 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Tue Oct 04 2016 00:15:23 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6ae21944d175b7874',
        'registered': 'Tue Dec 01 2015 10:29:55 GMT+0000',
        'username': 'Corina',
        'type': 'Enterprise',
        'email': 'corina@boink.io',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Thu Apr 26 2018 17:06:12 GMT+0000',
          'type': 'Growth',
          'amount': '$3,107.79',
          'cancellation': {
            'date': 'Tue Mar 01 2016 14:27:47 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sun Mar 15 2015 02:52:19 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Thu Nov 13 2014 08:08:58 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6e306281418c51853',
        'registered': 'Wed Jun 11 2014 21:12:46 GMT+0000',
        'username': 'Lindsay',
        'type': 'Enterprise',
        'email': 'lindsay@digitalus.ca',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Fri Feb 03 2017 05:30:17 GMT+0000',
          'type': 'Growth',
          'amount': '$3,059.77',
          'cancellation': {
            'date': 'Fri Oct 24 2014 15:38:24 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Fri Jun 13 2014 11:40:31 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Fri May 04 2018 16:32:32 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6aab405aecedfeb2c',
        'registered': 'Tue Nov 17 2015 15:21:51 GMT+0000',
        'username': 'Glenda',
        'type': 'Enterprise',
        'email': 'glenda@housedown.com',
        'profession': 'GYM',
        'plan': {
          'date': 'Thu Jul 27 2017 23:38:23 GMT+0000',
          'type': 'Growth',
          'amount': '$3,001.71',
          'cancellation': {
            'date': 'Wed Apr 08 2015 14:16:27 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu Apr 09 2015 07:41:30 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Thu Aug 14 2014 09:56:31 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6a75e6419a3c77eb1',
        'registered': 'Tue Aug 16 2016 11:15:50 GMT+0000',
        'username': 'Callahan',
        'type': 'Enterprise',
        'email': 'callahan@ontagene.info',
        'profession': 'GYM',
        'plan': {
          'date': 'Mon Jul 18 2016 15:19:42 GMT+0000',
          'type': 'Growth',
          'amount': '$2,478.42',
          'cancellation': {
            'date': 'Thu Dec 08 2016 17:16:17 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Tue Aug 08 2017 14:14:05 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Jun 18 2014 23:19:45 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6a44b6b14d9dcda2d',
        'registered': 'Sun Jun 08 2014 21:29:02 GMT+0000',
        'username': 'Burks',
        'type': 'Enterprise',
        'email': 'burks@aquasseur.tv',
        'profession': 'GYM',
        'plan': {
          'date': 'Tue Jan 28 2014 19:04:10 GMT+0000',
          'type': 'Enterprise',
          'amount': '$3,319.41',
          'cancellation': {
            'date': 'Fri Aug 04 2017 03:52:26 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sat Nov 25 2017 09:01:34 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Sat Feb 13 2016 19:09:08 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6e22d1d103309de6b',
        'registered': 'Thu Jan 07 2016 08:03:45 GMT+0000',
        'username': 'Craft',
        'type': 'Enterprise',
        'email': 'craft@songbird.name',
        'profession': 'GYM',
        'plan': {
          'date': 'Sun May 20 2018 01:43:52 GMT+0000',
          'type': 'Enterprise',
          'amount': '$1,187.76',
          'cancellation': {
            'date': 'Sun Dec 17 2017 22:15:41 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sun Feb 26 2017 09:16:03 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Fri Jan 31 2014 17:04:48 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e643f401653eb176ac',
        'registered': 'Sat Apr 14 2018 01:49:39 GMT+0000',
        'username': 'Burt',
        'type': 'Enterprise',
        'email': 'burt@eclipto.co.uk',
        'profession': 'GYM',
        'plan': {
          'date': 'Sat Dec 02 2017 04:26:58 GMT+0000',
          'type': 'Enterprise',
          'amount': '$2,969.64',
          'cancellation': {
            'date': 'Mon Aug 11 2014 09:30:02 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Tue Feb 06 2018 10:43:48 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Fri Oct 14 2016 19:41:41 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e656856cb5c8888cc6',
        'registered': 'Tue Oct 06 2015 14:37:17 GMT+0000',
        'username': 'Hodge',
        'type': 'Enterprise',
        'email': 'hodge@sloganaut.org',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Sun Mar 27 2016 11:00:24 GMT+0000',
          'type': 'Growth',
          'amount': '$3,518.42',
          'cancellation': {
            'date': 'Sun Mar 27 2016 23:28:14 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sun Jun 28 2015 23:01:08 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Mon Dec 25 2017 06:46:14 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e6383ebd3e196ff9d4',
        'registered': 'Sat Jul 18 2015 19:35:34 GMT+0000',
        'username': 'Jeanne',
        'type': 'Individual',
        'email': 'jeanne@insource.us',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Tue Dec 15 2015 09:53:26 GMT+0000',
          'type': 'Enterprise',
          'amount': '$3,840.68',
          'cancellation': {
            'date': 'Tue Dec 12 2017 13:24:22 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sun Jul 17 2016 10:25:23 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Wed Jul 19 2017 23:29:55 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e61415e63ee10003b9',
        'registered': 'Tue Dec 19 2017 09:06:39 GMT+0000',
        'username': 'Magdalena',
        'type': 'Enterprise',
        'email': 'magdalena@pyramis.net',
        'profession': 'HO air ballon',
        'plan': {
          'date': 'Tue Jan 28 2014 08:27:55 GMT+0000',
          'type': 'Growth',
          'amount': '$1,171.91',
          'cancellation': {
            'date': 'Thu May 08 2014 16:04:33 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Thu Apr 26 2018 10:20:27 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Fri Feb 27 2015 05:14:02 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e66c9cd23b59bb3e7d',
        'registered': 'Tue Jun 07 2016 02:39:51 GMT+0000',
        'username': 'Kaye',
        'type': 'Enterprise',
        'email': 'kaye@overplex.me',
        'profession': 'Salon',
        'plan': {
          'date': 'Thu Mar 20 2014 18:11:07 GMT+0000',
          'type': 'Enterprise',
          'amount': '$3,481.30',
          'cancellation': {
            'date': 'Tue Aug 29 2017 04:06:39 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Sun Jun 26 2016 02:50:02 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Tue Mar 31 2015 15:54:14 GMT+0000'
            }
          ]
        }
      },
      {
        'id': '5b35f5e60dfefd70b2e6c80f',
        'registered': 'Wed Jun 22 2016 20:49:36 GMT+0000',
        'username': 'Espinoza',
        'type': 'Individual',
        'email': 'espinoza@tubesys.biz',
        'profession': 'GYM',
        'plan': {
          'date': 'Mon Oct 12 2015 14:55:02 GMT+0000',
          'type': 'Enterprise',
          'amount': '$3,961.49',
          'cancellation': {
            'date': 'Wed Feb 21 2018 04:14:06 GMT+0000'
          },
          'activity': [
            {
              'type': 'upgrade',
              'date': 'Wed Aug 19 2015 12:49:20 GMT+0000'
            },
            {
              'type': 'downgrade',
              'date': 'Fri Aug 12 2016 21:27:44 GMT+0000'
            }
          ]
        }
      }
    ]
  }
  ;

  paymentData = {
    'header': [{
      'title': 'Id',
      'key': 'id'
    },
      {
        'title': 'Username',
        'key': 'username'
      },
      {
        'title': 'Date',
        'key': 'date'
      },
      {
        'title': 'Service',
        'key': 'service'
      },
      {
        'title': 'Amount',
        'key': 'amount'
      },
      {
        'title': 'Location',
        'key': 'location'
      }],
    'rows': [
      {
        'id': '5b35f76984bbadd55233ce4d',
        'username': 'Stark',
        'date': 'Thu Nov 05 2015 12:34:48 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$462.64',
        'location': 'Jillian'
      },
      {
        'id': '5b35f76940e0302689070373',
        'username': 'Rich',
        'date': 'Sun Jan 17 2016 21:30:13 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$478.07',
        'location': 'Willis'
      },
      {
        'id': '5b35f769b83d61c09ea55cf4',
        'username': 'Chavez',
        'date': 'Wed Jan 03 2018 07:14:06 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$168.74',
        'location': 'Jody'
      },
      {
        'id': '5b35f769a585cef16abe577a',
        'username': 'Bonita',
        'date': 'Sun Aug 17 2014 12:25:55 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$492.14',
        'location': 'Clarice'
      },
      {
        'id': '5b35f769b1ef7674866a6626',
        'username': 'Rhodes',
        'date': 'Thu Aug 31 2017 15:01:36 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$341.30',
        'location': 'Compton'
      },
      {
        'id': '5b35f7696ff785f13c63e0a4',
        'username': 'Morris',
        'date': 'Mon Dec 08 2014 15:12:44 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$114.43',
        'location': 'Kirkland'
      },
      {
        'id': '5b35f76958bc24ab3ac97815',
        'username': 'Hamilton',
        'date': 'Wed Jun 04 2014 13:02:22 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$173.41',
        'location': 'Bradley'
      },
      {
        'id': '5b35f7695adc4817d211a469',
        'username': 'Nancy',
        'date': 'Sun Aug 13 2017 13:51:31 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$381.27',
        'location': 'Hope'
      },
      {
        'id': '5b35f7690d7d7aba45451f6b',
        'username': 'Cleveland',
        'date': 'Fri Aug 01 2014 13:42:48 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$128.79',
        'location': 'Avis'
      },
      {
        'id': '5b35f769ebb833c38f449c7b',
        'username': 'Sheryl',
        'date': 'Tue Mar 14 2017 23:12:34 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$480.39',
        'location': 'Patel'
      },
      {
        'id': '5b35f769e506d226dcc7a093',
        'username': 'Helen',
        'date': 'Sun Jun 26 2016 02:55:02 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$103.26',
        'location': 'Hanson'
      },
      {
        'id': '5b35f769e8f8030746ecd977',
        'username': 'Johnston',
        'date': 'Thu Dec 03 2015 15:41:06 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$254.86',
        'location': 'Moreno'
      },
      {
        'id': '5b35f7698b3287f245d3739f',
        'username': 'Mcclain',
        'date': 'Tue Aug 29 2017 12:00:09 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$332.98',
        'location': 'Velez'
      },
      {
        'id': '5b35f769d5b5048a3c4e7aa7',
        'username': 'Barr',
        'date': 'Mon May 05 2014 20:52:12 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$210.55',
        'location': 'Henrietta'
      },
      {
        'id': '5b35f7694aad81ee2e932e1c',
        'username': 'Nixon',
        'date': 'Fri Oct 21 2016 10:25:54 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$341.90',
        'location': 'Marci'
      },
      {
        'id': '5b35f7692a92fb4501cdbc1c',
        'username': 'Sarah',
        'date': 'Wed Jan 17 2018 02:06:16 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$432.64',
        'location': 'Britt'
      },
      {
        'id': '5b35f76939a54714c46f68e7',
        'username': 'Velma',
        'date': 'Wed Jul 22 2015 22:15:17 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$153.71',
        'location': 'Goff'
      },
      {
        'id': '5b35f76980997bf721a88263',
        'username': 'Gomez',
        'date': 'Mon Jun 04 2018 23:52:18 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$299.14',
        'location': 'Josefina'
      },
      {
        'id': '5b35f769b620d4cbbd85d124',
        'username': 'Solomon',
        'date': 'Thu Oct 19 2017 20:07:55 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$305.19',
        'location': 'Hardy'
      },
      {
        'id': '5b35f769a80ee3944c3499a9',
        'username': 'Kristina',
        'date': 'Tue Apr 03 2018 06:40:10 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$432.68',
        'location': 'Frederick'
      },
      {
        'id': '5b35f769e3d540b65a3e849d',
        'username': 'Aurelia',
        'date': 'Thu Apr 12 2018 11:30:18 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$325.84',
        'location': 'Cassandra'
      },
      {
        'id': '5b35f7694c268717fa68fb22',
        'username': 'Emerson',
        'date': 'Wed Feb 26 2014 18:30:21 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$157.61',
        'location': 'Robbie'
      },
      {
        'id': '5b35f769e7c263c47e052d48',
        'username': 'Vilma',
        'date': 'Thu Sep 07 2017 22:26:32 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$139.62',
        'location': 'Branch'
      },
      {
        'id': '5b35f7691e2a8d4cc98464a9',
        'username': 'Wilkinson',
        'date': 'Wed Feb 12 2014 20:40:38 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$364.44',
        'location': 'Carver'
      },
      {
        'id': '5b35f769d4258f4c86567aa5',
        'username': 'Lindsay',
        'date': 'Thu Feb 11 2016 15:58:17 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$376.38',
        'location': 'Russell'
      },
      {
        'id': '5b35f769301c7b5fdbd81bb9',
        'username': 'Ericka',
        'date': 'Fri Feb 16 2018 23:38:22 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$363.89',
        'location': 'Gallagher'
      },
      {
        'id': '5b35f769efd03769c97786ed',
        'username': 'Maryanne',
        'date': 'Sat Mar 19 2016 11:25:09 GMT+0000',
        'service': [
          'hair cut',
          'hair color'
        ],
        'amount': '$362.74',
        'location': 'Lenore'
      }
    ]
  };

  ngOnInit() {
    if (this.data) {
      this.render();
    }
    if (this.paymentData) {
      this.render();
    }
  }

  render() {
    this.configObject.settings = [];
    this.configObject.fields = [];
    if (!this.data) {
      return;
    }
    this.configObject.data = [...this.data.rows];
    this.data.header.forEach(col => {
      this.configObject.settings = [
        ...this.configObject.settings,
        {
          objectKey: col.key
        }
      ];
      this.configObject.fields = [
        ...this.configObject.fields,
        {
          objectKey: col.key,
          name: col.title,
          render: r => {
            const x = col.key.split('.');
            if (x.length === 1) {
              return r[x[0]];
            } else if (x.length === 2) {
              return r[x[0]][x[1]];
            } else if (x.length === 3) {
              return r[x[0]][x[1]][x[2]];
            }
          },
          value: r => {
            const x = col.key.split('.');
            if (x.length === 1) {
              return r[x[0]];
            } else if (x.length === 2) {
              return r[x[0]][x[1]];
            } else if (x.length === 3) {
              return r[x[0]][x[1]][x[2]];
            }
          }
        }
      ];
    });






    this.paymentObject.settings = [];
    this.paymentObject.fields = [];
    if (!this.paymentData) {
      return;
    }
    this.paymentObject.data = [...this.paymentData.rows];
    this.paymentData.header.forEach(col => {
      this.paymentObject.settings = [
        ...this.paymentObject.settings,
        {
          objectKey: col.key
        }
      ];
      this.paymentObject.fields = [
        ...this.paymentObject.fields,
        {
          objectKey: col.key,
          name: col.title,
          render: r => {
            const x = col.key.split('.');
            if (x.length === 1) {
              return r[x[0]];
            } else if (x.length === 2) {
              return r[x[0]][x[1]];
            } else if (x.length === 3) {
              return r[x[0]][x[1]][x[2]];
            }
          },
          value: r => {
            const x = col.key.split('.');
            if (x.length === 1) {
              return r[x[0]];
            } else if (x.length === 2) {
              return r[x[0]][x[1]];
            } else if (x.length === 3) {
              return r[x[0]][x[1]][x[2]];
            }
          }
        }
      ];
    });


  }



}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) {
  }

  getRegistered() {
    return this.http.get('/regis.json');
  }

  getPayment() {
    return this.http.get('/payment.json');
  }
}
